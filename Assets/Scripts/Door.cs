﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : MonoBehaviour
{
    bool canInteract = false;
    bool triggered = false;
    public Animator anim;
    public bool open = false;

    public List<Transform> nodePathIn;
    public List<Transform> nodePathOut;

    private void OnTriggerEnter(Collider c)
    {
        if (triggered == true) return;
        if (c.tag == "Player")
        {
            UIManager.instance.SetTextInteract("Door");
            canInteract = true;
        }
    }
    private void OnTriggerExit(Collider c)
    {
        if (triggered == true) return;
        if (c.tag == "Player")
        {
            UIManager.instance.SetTextInteract("");
            canInteract = false;
        }
    }

    private void Update()
    {
        if (canInteract == false || triggered == true) return;

        if (Input.GetKeyDown(KeyCode.Joystick1Button0) || Input.GetMouseButtonDown(0))
        {
            anim.SetTrigger("Trigger");

            triggered = true;
            canInteract = false;
            open = !open;

            if (open) CameraManager.instance.NodePathCamera(nodePathIn);
            else CameraManager.instance.NodePathCamera(nodePathOut);
        }
    }

    public void ResetInteract()
    {
        
    }
}
