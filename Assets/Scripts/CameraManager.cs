﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum CameraMode
{ 
    FIRSTPERSON,
    THIRDPERSON,
    CCTVROOM,
    DYNAMICELYSIUM,
    FREELOOK,
    FIXED
}

public class CameraManager : MonoBehaviour
{
    public static CameraManager instance;
    private Camera activeCamera;

    [Header("Setup")]
    public bool hardSnap = false;
    public Transform playerTarget;
    public UnityStandardAssets.Characters.ThirdPerson.ThirdPersonUserControl input;

    [Header("Transition Camera")]
    public Camera TransitionCamera;
    public float transitionSpeed;
    public float nodeTransitionSpeed;
    public float variableSpeed;
    private bool changeCamera = false;
    private Transform targetPos;

    [Header("Third Person Camera")]
    public Camera thirdPersonCamera;
    public Vector3 thirdPersonOffset = Vector2.zero;
    private Vector3 playerCamDefaultPos = Vector3.zero;

    [Header("Security Camera")]
    public Camera cctvCamera;

    [Header("Isometric Camera")]
    public Camera isometricCamera;

    [Header("Elysium Style Camera")]
    public Camera elysiumCamera;

    private List<Transform> nodePath =  new List<Transform>();
    private Transform nodeTarget;

    private void Awake()
    {
        if (instance == null) instance = this;
        else Destroy(this.gameObject);

        activeCamera = thirdPersonCamera;
        input.InputRefCamera = activeCamera.transform;
        UIManager.instance.SetTextCamera(hardSnap.ToString());
        //TransitionToCamera(CameraMode.THIRDPERSON);
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space) || UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager.GetButtonDown("Jump"))
        {
            hardSnap = !hardSnap;
            UIManager.instance.SetTextCamera(hardSnap.ToString());
        }
    }

    private void LateUpdate()
    {
        TransitionCamera.transform.LookAt(playerTarget);

        if (changeCamera)
        {
            TransitionCamera.fieldOfView = Mathf.Lerp(TransitionCamera.fieldOfView, activeCamera.fieldOfView, Time.deltaTime * variableSpeed);
            TransitionCamera.transform.position = Vector3.Lerp(TransitionCamera.transform.position, targetPos.position, Time.deltaTime * transitionSpeed);
            if (Vector3.Distance(targetPos.position, TransitionCamera.transform.position) < 0.1f)
            {
                CompleteTransition();
            }
        }

        if (nodePath.Count > 0)
        {
            TransitionCamera.transform.position = Vector3.LerpUnclamped(TransitionCamera.transform.position, nodeTarget.position, Time.deltaTime * transitionSpeed);
            if (Vector3.Distance(nodeTarget.position, TransitionCamera.transform.position) < 0.3f)
            {
                if (nodePath.Count > 1)
                {
                    nodeTarget = nodePath[1];
                }
                else
                {
                    CompleteTransition();
                    nodeTarget = null;
                    // Last target reached
                }
                nodePath.RemoveAt(0);
            }
        }
    }

    public void TransitionToCamera(CameraMode cameraMode, Transform targetPos)
    {
        // Enable the transitional camera'
        TransitionCamera.transform.position = activeCamera.transform.position;
        TransitionCamera.fieldOfView = activeCamera.fieldOfView;
        TransitionCamera.gameObject.SetActive(true);
        input.InputRefCamera = TransitionCamera.transform;
        this.targetPos = targetPos;

        // Hide the active Camera
        activeCamera.gameObject.SetActive(false);

        // Change the active camera to the one we are going to select
        switch (cameraMode)
        {
            case CameraMode.FIRSTPERSON:
                break;
            case CameraMode.THIRDPERSON:
                activeCamera = thirdPersonCamera;
                break;
            case CameraMode.CCTVROOM:
                input.readInput = false; // during camera transition disable movement
                cctvCamera.transform.position = targetPos.position;
                activeCamera = cctvCamera;
                break;
            case CameraMode.FREELOOK:
                input.readInput = false; // during camera transition disable movement
                isometricCamera.transform.root.position = targetPos.position;
                isometricCamera.transform.forward = playerTarget.position - isometricCamera.transform.root.position;
                activeCamera = isometricCamera;
                break;
            case CameraMode.DYNAMICELYSIUM:
                input.readInput = false;
                break;
            case CameraMode.FIXED:
                break;
            default:
                break;
        }

        // Start moving the transition camera into position
        if (hardSnap == false) changeCamera = true;
        else CompleteTransition();
    }

    /// <summary>
    /// Transition to third person or first person camera modes
    /// </summary>
    /// <param name="cameraMode"></param>
    public void TransitionToCamera(CameraMode cameraMode)
    {
        TransitionToCamera(cameraMode, thirdPersonCamera.transform);
    }

    private void CompleteTransition()
    {
        activeCamera.gameObject.SetActive(true);
        TransitionCamera.gameObject.SetActive(false);
        input.InputRefCamera = activeCamera.transform;
        input.readInput = true;
        changeCamera = false;
    }

    public void NodePathCamera(List<Transform> nodes)
    {
        // Enable the transitional camera'
        TransitionCamera.transform.position = activeCamera.transform.position;
        TransitionCamera.fieldOfView = activeCamera.fieldOfView;
        TransitionCamera.gameObject.SetActive(true);
        
        // Remove active camera
        activeCamera.gameObject.SetActive(false);

        // Set elysium camera as next active
        elysiumCamera.transform.root.position = nodes[nodes.Count-1].position;
        activeCamera = elysiumCamera;

        for (int i=0; i<nodes.Count; i++)
        {
            this.nodePath.Add(nodes[i]);
        }

        // Initial target
        nodeTarget = nodePath[0];
    }
}