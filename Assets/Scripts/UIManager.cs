﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIManager : MonoBehaviour
{
    public static UIManager instance;
    private void Awake()
    {
        if (instance == null) instance = this;
        else Destroy(this.gameObject);
    }

    public TMPro.TextMeshProUGUI interactText;
    public TMPro.TextMeshProUGUI cameraModeText;

    private const string INTERACT = "Interactable: ";
    private const string CAMERA = "Hard Snapping: ";


    public void SetTextInteract(string interactable)
    {
        interactText.text = INTERACT + interactable;
    }

    public void SetTextCamera(string interactable)
    {
        cameraModeText.text = CAMERA + interactable;
    }
}
