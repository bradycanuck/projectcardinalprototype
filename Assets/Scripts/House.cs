﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class House : MonoBehaviour
{
    public enum HouseType
    { 
        Security,
        Freelook
    }
    public HouseType houseType;
    public bool hardSnapCamera;

    public Transform CameraTargetPosition;

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            switch (houseType)
            {
                case HouseType.Security:
                    CameraManager.instance.TransitionToCamera(CameraMode.CCTVROOM, CameraTargetPosition);
                    break;
                case HouseType.Freelook:
                    CameraManager.instance.TransitionToCamera(CameraMode.FREELOOK, CameraTargetPosition);
                    break;
                default:
                    break;
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if(other.tag == "Player")
            CameraManager.instance.TransitionToCamera(CameraMode.THIRDPERSON);
    }
}
